## Fork of [docker-postgres-wale](https://github.com/lukesmith/docker-postgres-wale)

# Postgres docker container with wale

**_Note:_** The main container without a slave is working in production, however at the moment the slave remians untested.  I restore with a seperate machine as documented below.  

Based on https://github.com/docker-library/postgres with [WAL-E](https://github.com/wal-e/wal-e) installed.

Environment variables to pass to the container for WAL-E, all of these must be present or WAL-E is not configured.

`docker pull registry.gitlab.com/solarisoffgrid-opensource/docker-postgres-wale-azure`

```
WALE_WABS_PREFIX="wabs://<bucketname>/<path>"
WABS_ACCOUNT_NAME
WABS_ACCESS_KEY
WABS_SAS_TOKEN (optional)
```

## Running

The master

```
docker run -it \
  --env "WABS_ACCOUNT_NAME=****" \
  --env "WABS_ACCESS_KEY=****" \
  --env "WALE_WABS_PREFIX=wabs://my-bucket" \
  --env "POSTGRES_AUTHORITY=master" \
  -v ./data/master:/var/lib/postgresql/data \
  docker-postgres-azure
```

The slave will run in `standby_mode`.

```
docker run -it \
  --env "WABS_ACCOUNT_NAME=****" \
  --env "WABS_ACCESS_KEY=****" \
  --env "WALE_WABS_PREFIX=wabs://my-bucket" \
  --env "POSTGRES_AUTHORITY=slave" \
  -v ./data/slave:/var/lib/postgresql/data \
  docker-postgres-azure
```

When bringing online `rm ./data/recovery.conf` and start the container with `POSTGRES_AUTHORITY=master`.

## Restoring on a seperate machine

Links I used for reference:
* [Good overview](https://edwardsamuel.wordpress.com/2016/06/02/restoring-wal-e-postgresql-database-backup/)
* [Full guide on Ubuntu 12](https://gist.github.com/elithrar/8682235)
* [Installing postgres](https://askubuntu.com/questions/831292/how-to-install-postgresql-9-6-on-any-ubuntu-version)
* [The Official wal-e docs](https://github.com/wal-e/wal-e#backup-fetch)

**_Note:_ I ran a restore process on a seperate Ubuntu 16.04 machine, and the order of installing dependencies for a successful restore was very important.**

#### Getting some of the necessary machine dependencies.
```
$ apt-get update && apt-get install -y python3-pip python3 lzop pv daemontools
$ pip3 install --upgrade pip
$ python3 -m pip install wal-e[azure]
$ pip3 install azure-storage==0.20.0 <-- As of this posting there is no fix for the new Azure BlobStorage API in the wal-e package
```

#### Setting all the environment variables

```
$ umask u=rwx,g=rx,o=
$ mkdir -p /etc/wal-e.d/env
$ echo "secret-key-content" > /etc/wal-e.d/env/WABS_ACCESS_KEY
$ echo "access-key" > /etc/wal-e.d/env/WABS_ACCOUNT_NAME
$ echo 'wabs://some-container/directory/or/whatever' > \
  /etc/wal-e.d/env/WALE_WABS_PREFIX
```
#### Installing PostgreSQL
```
$ sudo add-apt-repository "deb http://apt.postgresql.org/pub/repos/apt/ xenial-pgdg main"
$ wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
$ sudo apt-get update
$ sudo apt-get install postgresql-9.6
$ chown -R root:postgres /etc/wal-e.d
```
#### You need to stop PostgreSQL and remove its data directory.
```
$ sudo service postgresql stop
$ sudo rm -rf /var/lib/postgresql/9.6/main
```
#### As user `postgres`, you can then `backup-fetch` from your storage location.  
```
$ sudo su - postgres
$ envdir /etc/wal-e.d/env /usr/local/bin/wal-e backup-fetch /var/lib/postgresql/9.6/main LATEST
```
#### When PostgreSQL starts up again, it will read the `recovery.conf` file to 
```
$ nano /var/lib/postgresql/9.6/main/recovery.conf
```
 #### `recovery.conf`
 ```
 # The command that will be executed while recovering
restore_command = 'envdir /etc/wal-e.d/env /usr/local/bin/wal-e wal-fetch "%f" "%p"'
 
# You can specify this value with exact time. This will be useful if
# you have incident and you want to recover to a few moments before.
# recovery_target_time = '2016-06-02 06:18:00'
```

#### Starting up PostgreSQL

```
$ exit
$ sudo service postgresql start
  ```
  
 The startup process may take a while depending on the size of the database.  
 
 If you think something is going wrong, you can see the stdout at `var/log/syslog`


### If you need to run a backup manually in the container

```
$ docker exec -it <postgres-container-name> bash
$ su - postgres
$ /usr/bin/envdir /etc/wal-e.d/env /usr/local/bin/wal-e backup-push /var/lib/postgresql/data
```

